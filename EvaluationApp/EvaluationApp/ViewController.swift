//
//  ViewController.swift
//  EvaluationApp
//
//  Created by Steven Cuasqui on 11/27/18.
//  Copyright © 2018 Steven Cuasqui. All rights reserved.
//

import UIKit
import FirebaseAuth

class ViewController: UIViewController {

    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func buttonLogin(_ sender: Any) {
        
        let user = userEmail.text!
        let password = userPassword.text!
        
        Auth.auth().signIn(withEmail: user, password: password){ (data, error) in
            
            if let err = error {
                print(err)
                return
            }
            
            self.performSegue(withIdentifier: "numberSegue", sender: self)
        }
        
        
    }


}

