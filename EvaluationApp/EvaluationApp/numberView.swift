//
//  numberView.swift
//  EvaluationApp
//
//  Created by Steven Cuasqui on 11/27/18.
//  Copyright © 2018 Steven Cuasqui. All rights reserved.
//

import UIKit

class numberView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var numbersLabel: UILabel!
    @IBOutlet weak var nText: UITextField!
    
    
    @IBAction func buttonPares(_ sender: Any) {
        
        let count = nText.text!
        let countInt = Int(count)
        print(count)
        
        let numbersResult=pairsNumbers(count: countInt!)
        print(numbersResult)
        numbersLabel.text = "\(numbersResult)"


    }
    
    func pairsNumbers(count:Int) -> [Int]{
        
        var numbers=[Int]()
        var i = 0
        while(i<count){
            numbers.append(i*2);
            i=i+1
        }
        return numbers
    }
    
    
    @IBAction func buttonReturn(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
